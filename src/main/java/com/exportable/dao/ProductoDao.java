package com.exportable.dao;

import com.exportable.models.Categoria;
import com.exportable.models.Producto;
import com.exportable.utils.Dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author jhonatan.acevedousam
 */
public class ProductoDao implements Dao<Producto> {

    @Autowired
    Connection getConex;
    private PreparedStatement p;
    private String[] ar = {
        "insert into producto(nombre,precio,stock,id_categoria) values(?,?,?,?)",
        "select * from producto order by id_producto"};

    private Producto pro;
    private List<Producto> lp;

    public ProductoDao() {
    }

    @Override
    public void create(Producto t) {
        try {
            Categoria c = t.getId_categoria();
            p = getConex.prepareStatement(ar[0]);
            p.setString(1, t.getNombre());
            p.setDouble(2, t.getPrecio());
            p.setInt(3, t.getStock());
            p.setString(4, c.getNombre());
            p.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en producto: Al crear");
        }
    }

    @Override
    public List<Producto> read() {
        try {
            Categoria c = new Categoria();
            p = getConex.prepareStatement(ar[1]);
            ResultSet rs = p.executeQuery();
            lp = new ArrayList<Producto>();
            while (rs.next()) {
                pro = new Producto();
                pro.setId_producto(rs.getInt("id_producto"));
                pro.setNombre(rs.getString("nombre"));
                pro.setPrecio(rs.getDouble("precio"));
                pro.setStock(rs.getInt("stock"));
                c.setId_categoria(rs.getInt("id_categoria"));
                pro.setId_categoria(c);
                lp.add(pro);
            }
            return lp;
        } catch (Exception e) {
            System.out.println("Error en producto: Al leer");
            return null;
        }
    }

}
