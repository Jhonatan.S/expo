package com.exportable.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author jhonatan.acevedousam
 */
public class Enlace {

    private static Connection c;

    public static Connection conecta() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            c = DriverManager.getConnection("jdbc:mysql://usam-sql.sv.cds:3306/facturacion?useSSL=false", "kz", "kzroot");
            PreparedStatement p = c.prepareStatement("select * from cliente order by id_cliente");
            ResultSet r = p.executeQuery();
        } catch (SQLException e) {
            c = null;
        } catch (ClassNotFoundException x) {
            c = null;
        }
        return c;
    }

    public static void cierra() {
        try {
            if (c != null) {
                if (!c.isClosed()) {
                    c.close();
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error al cerrar la conexion: " + ex.toString());
        }
    }
}
