package com.exportable.utils;

import java.util.List;

/**
 *
 * @author jhonatan.acevedousam
 */
public interface Dao<T> {

    public void create(T t);

    public List<T> read();
}
