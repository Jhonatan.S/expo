package com.exportable.models;

import java.util.Date;

/**
 *
 * @author jhonatan.acevedousam
 */
public class Factura {

    private int num_factura;
    private String codigo;
    private Date fecha;
    private Cliente id_cliente;
    private ModoPago num_pago;

    public Factura() {
    }

    public int getNum_factura() {
        return num_factura;
    }

    public void setNum_factura(int num_factura) {
        this.num_factura = num_factura;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Cliente getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Cliente id_cliente) {
        this.id_cliente = id_cliente;
    }

    public ModoPago getNum_pago() {
        return num_pago;
    }

    public void setNum_pago(ModoPago num_pago) {
        this.num_pago = num_pago;
    }

}
