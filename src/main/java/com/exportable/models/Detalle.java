package com.exportable.models;

/**
 *
 * @author jhonatan.acevedousam
 */
public class Detalle {

    private int num_detalle;
    private Factura num_factura;
    private int cantidad;
    private double precio;
    private Producto id_producto;

    public Detalle() {
    }

    public int getNum_detalle() {
        return num_detalle;
    }

    public void setNum_detalle(int num_detalle) {
        this.num_detalle = num_detalle;
    }

    public Factura getNum_factura() {
        return num_factura;
    }

    public void setNum_factura(Factura num_factura) {
        this.num_factura = num_factura;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Producto getId_producto() {
        return id_producto;
    }

    public void setId_producto(Producto id_producto) {
        this.id_producto = id_producto;
    }

}
